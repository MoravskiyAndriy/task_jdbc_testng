package com.moravskiyandriy.logo;

import com.moravskiyandriy.logo.pixels.implementation.BlackPixel;
import com.moravskiyandriy.logo.pixels.implementation.GreenPixel;
import com.moravskiyandriy.logo.pixels.implementation.WhitePixel;

import static com.moravskiyandriy.logo.Color.*;

public class Painter {
    public void paintLogo() {
        Color[][] smile = {
                {G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G},
                {G, W, W, W, W, W, W, G, W, W, W, G, W, W, W, G, W, G, W, G, W, G, W, W, W, G, W, W, W, G, B, B, B, G, B, G, B, G},
                {G, G, G, W, G, G, W, G, W, G, W, G, W, G, W, G, W, G, W, G, W, G, W, G, W, G, G, W, G, G, G, G, B, G, B, G, B, G},
                {G, G, G, G, W, G, W, G, W, W, W, G, W, W, W, G, W, G, W, G, W, G, W, W, W, G, G, W, G, G, B, B, B, G, B, B, B, G},
                {G, B, B, G, G, W, W, G, W, G, G, G, W, W, G, G, W, G, W, G, W, G, W, G, W, G, G, W, G, G, G, G, B, G, G, G, B, G},
                {G, B, B, G, G, G, W, G, W, G, G, G, W, G, W, G, W, G, G, W, G, G, W, G, W, G, G, W, G, G, B, B, B, G, G, G, B, G},
                {G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G}
        };

        Painting painting = new Painting(new WhitePixel());
        for (Color[] colors : smile) {
            for (Color color : colors) {
                switch (color) {
                    case W:
                        painting = new Painting(new WhitePixel());
                        break;
                    case G:
                        painting = new Painting(new GreenPixel());
                        break;
                    case B:
                        painting = new Painting(new BlackPixel());
                        break;
                }
                System.out.print(painting.getPixel());
            }
            System.out.println();
        }
    }
}
