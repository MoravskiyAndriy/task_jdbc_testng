package com.moravskiyandriy.logo.pixels.implementation;

import com.moravskiyandriy.logo.pixels.Pixel;

public class GreenPixel implements Pixel {

    @Override
    public String getColorPixel() {
        return "\033[0;92m" + " █";
    }
}
