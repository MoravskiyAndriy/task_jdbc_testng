package com.moravskiyandriy.controller.implementation;

import com.moravskiyandriy.businessLogic.AuthenticationManager;
import com.moravskiyandriy.controller.AuthenticationController;
import com.moravskiyandriy.dto.UserDTO;
import com.moravskiyandriy.entities.User;

public class AuthenticationControllerImpl implements AuthenticationController {
    private final AuthenticationManager authenticationManager;

    public AuthenticationControllerImpl() {
        authenticationManager = new AuthenticationManager();
    }

    @Override
    public User registerUser(UserDTO user, char[] password) {
        return authenticationManager.registerUser(user, password);
    }

    @Override
    public User loginUser(String phoneNumber, char[] password) {
        return authenticationManager.loginUser(phoneNumber, password);
    }

    @Override
    public boolean phoneNumberAlreadyExists(String phoneNumber) {
        return authenticationManager.phoneNumberAlreadyExists(phoneNumber);
    }
}
