package com.moravskiyandriy.controller;

import com.moravskiyandriy.dto.UserDTO;
import com.moravskiyandriy.entities.User;

public interface AuthenticationController {
    User registerUser(UserDTO user, char[] password);

    User loginUser(String phoneNumber, char[] password);

    boolean phoneNumberAlreadyExists(String phoneNumber);
}
