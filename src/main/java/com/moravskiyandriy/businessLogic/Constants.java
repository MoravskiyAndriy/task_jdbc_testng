package com.moravskiyandriy.businessLogic;

import com.moravskiyandriy.entities.Currency;

import java.math.BigDecimal;

public class Constants {
    private Constants() {
    }

    public static final int FORMAT_CONSTANT = 2;
    static final int MAX_ACCOUNT_NUMBER = 3;
    public static final int MAX_DEPOSIT_NUMBER = 3;
    public static final int MAX_CREDIT_NUMBER = 3;
    public static final int DEPOSIT_OPERATION = 6;
    public static final int CREDIT_OPERATION = 6;
    public static final int TRANSFER_TO_THIS_BANK_ACCOUNT = 2;
    public static final int REPLENISH_PHONE_BALANCE = 3;
    public static final int TRANSFER_TO_ANOTHER_BANK_ACCOUNT = 4;
    public static final BigDecimal CREDIT_LIMIT = new BigDecimal(10000);
    public static final Currency UAH_Currency = new Currency(1, "UAH", new BigDecimal(1));
    static final BigDecimal CREDIT_RATE = new BigDecimal(20);
    static final int CREDIT_TERM_DAYS = 180;
    static final Long UKR_GAS_BANK_ACCOUNT_NUMBER = 8888888888888888L;
    public static final int TICKETS_NUMBER = 3;
}
