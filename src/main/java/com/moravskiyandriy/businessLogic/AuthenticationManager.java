package com.moravskiyandriy.businessLogic;

import com.moravskiyandriy.dto.UserDTO;
import com.moravskiyandriy.entities.User;
import com.moravskiyandriy.services.PasswordServices;
import com.moravskiyandriy.services.UserService;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Objects;

public class AuthenticationManager {
    public User loginUser(String phoneNumber, char[] password) {
        User user = null;
        try {
            user = new UserService().getUserByPhoneNumber(phoneNumber);
            if (new PasswordServices().getUserPassword(user).equals(new String(encrypt(password)))) {
                return user;
            }
            user = null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

    public boolean phoneNumberAlreadyExists(String phoneNumber) {
        boolean present;
        try {
            present = Objects.nonNull(new UserService().getUserByPhoneNumber(phoneNumber));
        } catch (SQLException e) {
            present = false;
        }
        return present;
    }

    public User registerUser(UserDTO userDTO, char[] password) {
        User user = new User(userDTO.getName(), userDTO.getSurname(), userDTO.getPatronymic(),
                userDTO.getEmail(), userDTO.getPhoneNumber(), userDTO.getSettlement(),
                userDTO.getStreet(), userDTO.getHouseNumber(), userDTO.getFlatNumber(),
                new ArrayList<>(), new ArrayList<>(), new ArrayList<>());
        try {
            new UserService().add(user);
            user = new UserService().getUserByPhoneNumber(user.getPhoneNumber());
            new PasswordServices().setUserPassword(user, encrypt(password));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

    public boolean rightUser(String phoneNumber, char[] password) {
        try {
            User user = new UserService().getUserByPhoneNumber(phoneNumber);
            if (new PasswordServices().getUserPassword(user).equals(new String(encrypt(password)))) {
                return true;
            }
        } catch (SQLException e) {
            return false;
        }
        return false;
    }

    private static char[] encrypt(char[] text) {
        int s = 10;
        StringBuilder result = new StringBuilder();
        for (char c : text) {
            if (Character.isUpperCase(c)) {
                char ch = (char) ((((int) c) + s - 65) % 26 + 65);
                result.append(ch);
            } else {
                char ch = (char) (((int) c +
                        s - 97) % 26 + 97);
                result.append(ch);
            }
        }
        return result.toString().toCharArray();
    }
}
