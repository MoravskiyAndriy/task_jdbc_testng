package com.moravskiyandriy.businessLogic;

import com.moravskiyandriy.bankdeposit.deposit.BankDeposit;
import com.moravskiyandriy.entities.Deposit;
import com.moravskiyandriy.entities.User;
import com.moravskiyandriy.services.DepositService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.util.*;

public class DepositManager {
    private static final int MAX_DEPOSIT_NUMBER = Optional.
            ofNullable(getProperties().getProperty("MAX_DEPOSIT_NUMBER")).
            map(Integer::valueOf).orElse(Constants.MAX_DEPOSIT_NUMBER);
    private static final Logger logger = LogManager.getLogger(DepositManager.class);

    public boolean addDeposit(BankDeposit deposit) {
        int depositNumber = MAX_DEPOSIT_NUMBER;
        try {
            depositNumber = new DepositService().getUserDepositsQuantityById(deposit.getUserId());
        } catch (SQLException e) {
            logger.info("Some issues with database occurred. We are trying to solve the problem...");
            e.printStackTrace();
        }
        if (depositNumber < MAX_DEPOSIT_NUMBER) {
            Deposit nDeposit = formDeposit(deposit);
            try {
                new DepositService().add(nDeposit);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return true;
        } else {
            return false;
        }
    }

    private Deposit formDeposit(BankDeposit deposit) {
        Deposit nDeposit = new Deposit();
        nDeposit.setUserId(deposit.getUserId());
        nDeposit.setSum(deposit.getSum());
        nDeposit.setCurrency(deposit.getCurrency());
        nDeposit.setRate(deposit.getRate().setScale(2, RoundingMode.HALF_UP));
        nDeposit.setOpeningDate(deposit.getOpeningDate());
        nDeposit.setClosingDate(deposit.getClosingDate());
        return nDeposit;
    }

    public List<Deposit> getUserDeposits(User user) {
        try {
            return new DepositService().getUserDepositsById(user.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    public boolean deleteDeposit(Deposit deposit) {
        int depositNumber = MAX_DEPOSIT_NUMBER;
        try {
            depositNumber = new DepositService().getUserDepositsQuantityById(deposit.getUserId());
        } catch (SQLException e) {
            logger.info("Some issues with database occurred. We are trying to solve the problem...");
            e.printStackTrace();
        }
        if (depositNumber != 0) {
            try {
                new DepositService().delete(deposit);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return true;
        } else {
            return false;
        }
    }

    private static Properties getProperties() {
        Properties prop = new Properties();
        try (InputStream input = AccountManager.class
                .getClassLoader().getResourceAsStream("config")) {
            if (input != null) {
                prop.load(input);
            }
        } catch (NumberFormatException ex) {
            if (Objects.nonNull(logger)) {
                logger.warn("NumberFormatException found.");
            }
        } catch (IOException ex) {
            if (Objects.nonNull(logger)) {
                logger.warn("IOException found.");
            }
        }
        return prop;
    }
}
