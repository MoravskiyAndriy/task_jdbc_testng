package com.moravskiyandriy;

import com.moravskiyandriy.view.View;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;

public class Application {
    private static final Logger logger = LogManager.getLogger(Application.class);

    public static void main(String[] args) throws SQLException {
        //logger.info("Metadata:");
        //Metadata.printGeneralMetadata();
        //Metadata.printTablesMetadata();
        new View().show();
    }
}
