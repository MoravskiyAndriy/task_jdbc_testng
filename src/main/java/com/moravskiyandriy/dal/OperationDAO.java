package com.moravskiyandriy.dal;

import com.moravskiyandriy.entities.Operation;

import java.sql.SQLException;

public interface OperationDAO {
    Operation getOperationById(int id) throws SQLException;

    void update(Operation operation) throws SQLException;

    void add(Operation operation) throws SQLException;

    void delete(Operation operation) throws SQLException;
}
