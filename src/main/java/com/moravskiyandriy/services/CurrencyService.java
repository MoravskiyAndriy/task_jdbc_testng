package com.moravskiyandriy.services;

import com.moravskiyandriy.dal.CurrencyDAO;
import com.moravskiyandriy.entities.Currency;
import com.moravskiyandriy.utils.Util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class CurrencyService extends Util implements CurrencyDAO {
    @Override
    public Currency getCurrencyByAbbreviation(String abbreviation) throws SQLException {
        Connection connection = getConnection();
        PreparedStatement preparedStatement = null;
        Currency currency = new Currency();
        String sql = "SELECT * FROM currency WHERE currency_val=?";
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, abbreviation);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            currency.setId(resultSet.getInt("id"));
            currency.setCurrencyVal(resultSet.getString("currency_val"));
            currency.setToUAHModifier(resultSet.getBigDecimal("to_UAH_modifier"));
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) preparedStatement.close();
            if (connection != null) connection.close();
        }
        return currency;
    }

    @Override
    public void update(Currency currency) throws SQLException {
        Connection connection = getConnection();
        PreparedStatement preparedStatement = null;
        String sql = "UPDATE currency SET" +
                "id=?,currency_val=?,to_UAH_modifier=? WHERE id=?;";
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(4, currency.getId());
            preparedStatement.setInt(1, currency.getId());
            preparedStatement.setString(2, currency.getCurrencyVal());
            preparedStatement.setBigDecimal(3, currency.getToUAHModifier());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) preparedStatement.close();
            if (connection != null) connection.close();
        }
    }

    public void add(Currency currency) throws SQLException {
        Connection connection = getConnection();
        PreparedStatement preparedStatement = null;
        String sql = "INSERT INTO currency " +
                "(id,currency_val,to_UAH_modifier) " +
                "VALUES (?,?,?)";
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, currency.getId());
            preparedStatement.setString(2, currency.getCurrencyVal());
            preparedStatement.setBigDecimal(3, currency.getToUAHModifier());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) preparedStatement.close();
            if (connection != null) connection.close();
        }
    }

    public void delete(Currency currency) throws SQLException {
        Connection connection = getConnection();
        PreparedStatement preparedStatement = null;
        String sql = "DELETE FROM currency WHERE id=?";
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, currency.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) preparedStatement.close();
            if (connection != null) connection.close();
        }
    }

    @Override
    public List<Currency> getAllCurrency() throws SQLException {
        Connection connection = getConnection();
        PreparedStatement preparedStatement = null;
        List<Currency> currencies = new LinkedList<>();
        String sql = "SELECT * FROM currency";
        try {
            preparedStatement = connection.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();
            while(resultSet.next()) {
                Currency currency = new Currency();
                currency.setId(resultSet.getInt("id"));
                currency.setCurrencyVal(resultSet.getString("currency_val"));
                currency.setToUAHModifier(resultSet.getBigDecimal("to_UAH_modifier"));
                currencies.add(currency);
            }
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) preparedStatement.close();
            if (connection != null) connection.close();
        }
        return currencies;
    }
}
