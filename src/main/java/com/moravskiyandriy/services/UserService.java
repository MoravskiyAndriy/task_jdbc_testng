package com.moravskiyandriy.services;

import com.moravskiyandriy.dal.UserDAO;
import com.moravskiyandriy.entities.User;
import com.moravskiyandriy.utils.Util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserService extends Util implements UserDAO {
    @Override
    public void add(User user) throws SQLException {
        Connection connection = getConnection();
        PreparedStatement preparedStatement = null;
        String sql = "INSERT INTO user " +
                "(name,surname,patronymic,email,phone_number,settlement,street,house_number,flat_number) " +
                "VALUES (?,?,?,?,?,?,?,?,?)";
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, user.getName());
            preparedStatement.setString(2, user.getSurname());
            preparedStatement.setString(3, user.getPatronymic());
            preparedStatement.setString(4, user.getEmail());
            preparedStatement.setString(5, user.getPhoneNumber());
            preparedStatement.setString(6, user.getSettlement());
            preparedStatement.setString(7, user.getStreet());
            preparedStatement.setInt(8, user.getHouseNumber());
            preparedStatement.setInt(9, user.getFlatNumber());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) preparedStatement.close();
            if (connection != null) connection.close();
        }
    }

    @Override
    public User getUserByPhoneNumber(String number) throws SQLException {
        Connection connection = getConnection();
        PreparedStatement preparedStatement = null;
        String sql = "SELECT * FROM user WHERE phone_number=?";
        User user = new User();
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, number);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            formUser(number, user, resultSet);
            resultSet.close();
        } catch (SQLException e) {
            throw e;
        } finally {
            if (preparedStatement != null) preparedStatement.close();
            if (connection != null) connection.close();
        }
        return user;
    }

    private void formUser(String number, User user, ResultSet resultSet) throws SQLException {
        user.setId(resultSet.getInt("id"));
        user.setEmail(resultSet.getString("email"));
        user.setName(resultSet.getString("name"));
        user.setSurname(resultSet.getString("surname"));
        user.setPatronymic(resultSet.getString("patronymic"));
        user.setSettlement(resultSet.getString("settlement"));
        user.setPhoneNumber(number);
        user.setStreet(resultSet.getString("street"));
        user.setHouseNumber(resultSet.getInt("house_number"));
        user.setFlatNumber(resultSet.getInt("flat_number"));
        user.setAccounts(new AccountService().getUserAccountsById(user.getId()));
    }

    @Override
    public void update(User user) throws SQLException {
        Connection connection = getConnection();
        PreparedStatement preparedStatement = null;
        String sql = "UPDATE user SET id=?,name=?,surname=?,patronymic=?," +
                "email=?,phone_number=?,settlement=?,street=?,house_number=?,flat_number=? WHERE id=?";
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(11, user.getId());
            preparedStatement.setString(1, user.getName());
            preparedStatement.setString(2, user.getSurname());
            preparedStatement.setString(3, user.getPatronymic());
            preparedStatement.setString(4, user.getEmail());
            preparedStatement.setString(5, user.getPhoneNumber());
            preparedStatement.setString(6, user.getSettlement());
            preparedStatement.setString(7, user.getStreet());
            preparedStatement.setInt(8, user.getHouseNumber());
            preparedStatement.setInt(9, user.getFlatNumber());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) preparedStatement.close();
            if (connection != null) connection.close();
        }
    }
}
