package com.moravskiyandriy.services;

import com.moravskiyandriy.dal.DepositDAO;
import com.moravskiyandriy.entities.Currency;
import com.moravskiyandriy.entities.Deposit;
import com.moravskiyandriy.utils.Util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DepositService extends Util implements DepositDAO {
    @Override
    public List<Deposit> getUserDepositsById(int id) throws SQLException {
        Connection connection = getConnection();
        List<Deposit> deposits = new ArrayList<>();
        PreparedStatement preparedStatement = null;
        String sql = "SELECT * FROM deposit JOIN currency ON deposit.currency_id=currency.id WHERE user_id=?;";
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Deposit deposit = formDeposit(id, resultSet);
                deposits.add(deposit);
            }
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) preparedStatement.close();
            if (connection != null) connection.close();
        }
        return deposits;
    }

    private Deposit formDeposit(int id, ResultSet resultSet) throws SQLException {
        Deposit deposit = new Deposit();
        Currency currency = new Currency();
        deposit.setId(resultSet.getInt("id"));
        deposit.setSum(resultSet.getBigDecimal("sum"));
        deposit.setRate(resultSet.getBigDecimal("rate"));
        deposit.setOpeningDate(resultSet.getDate("opening_date"));
        deposit.setClosingDate(resultSet.getDate("closing_date"));
        deposit.setUserId(id);
        currency.setId(resultSet.getInt("currency_id"));
        currency.setCurrencyVal(resultSet.getString("currency_val"));
        currency.setToUAHModifier(resultSet.getBigDecimal("to_UAH_modifier"));
        deposit.setCurrency(currency);
        return deposit;
    }

    @Override
    public int getUserDepositsQuantityById(int id) throws SQLException {
        Connection connection = getConnection();
        PreparedStatement preparedStatement = null;
        int quantity = 0;
        String sql = "SELECT count(*) as number FROM deposit where user_id=?;";
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            quantity = resultSet.getInt("number");
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) preparedStatement.close();
            if (connection != null) connection.close();
        }
        return quantity;
    }

    @Override
    public void delete(Deposit deposit) throws SQLException {
        Connection connection = getConnection();
        PreparedStatement preparedStatement = null;
        String sql = "DELETE FROM deposit WHERE id=?;";
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setLong(1, deposit.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) preparedStatement.close();
            if (connection != null) connection.close();
        }
    }

    @Override
    public void add(Deposit deposit) throws SQLException {
        Connection connection = getConnection();
        PreparedStatement preparedStatement = null;
        String sql = "INSERT INTO deposit " +
                "(user_id,sum,currency_id,rate,opening_date,closing_date) " +
                "VALUES (?,?,?,?,?,?);";
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, deposit.getUserId());
            preparedStatement.setBigDecimal(2, deposit.getSum());
            preparedStatement.setInt(3, deposit.getCurrency().getId());
            preparedStatement.setBigDecimal(4, deposit.getRate());
            preparedStatement.setDate(5, deposit.getOpeningDate());
            preparedStatement.setDate(6, deposit.getClosingDate());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) preparedStatement.close();
            if (connection != null) connection.close();
        }
    }
}
