package com.moravskiyandriy.services;

import com.moravskiyandriy.dal.TransactionDAO;
import com.moravskiyandriy.entities.Account;
import com.moravskiyandriy.entities.Currency;
import com.moravskiyandriy.entities.Operation;
import com.moravskiyandriy.entities.Transaction;
import com.moravskiyandriy.utils.Util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class TransactionService extends Util implements TransactionDAO {
    @Override
    public List<Transaction> getAccountTransactions(Account account) throws SQLException {
        Connection connection = getConnection();
        PreparedStatement preparedStatement = null;
        List<Transaction> transactions = new LinkedList<>();
        String sql = "SELECT * FROM transaction AS t JOIN currency AS c ON t.currency_id=c.id JOIN operation AS o ON " +
                "t.operation_id=o.id WHERE t.account_number_this=?;";
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setLong(1, account.getNumber());
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Transaction transaction = formTransaction(resultSet);
                transactions.add(transaction);
            }
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) preparedStatement.close();
            if (connection != null) connection.close();
        }
        return transactions;
    }

    private Transaction formTransaction(ResultSet resultSet) throws SQLException {
        Transaction transaction = new Transaction();
        Currency currency = new Currency();
        Operation operation = new Operation();
        transaction.setId(resultSet.getInt("id"));
        transaction.setAccountNumberThis(resultSet.getLong("account_number_this"));
        transaction.setOperationCommentary(resultSet.getString("operation_commentary"));
        transaction.setSum(resultSet.getBigDecimal("sum"));
        transaction.setDate(resultSet.getTimestamp("date"));
        currency.setId(resultSet.getInt("currency_id"));
        currency.setCurrencyVal(resultSet.getString("currency_val"));
        currency.setToUAHModifier(resultSet.getBigDecimal("to_UAH_modifier"));
        transaction.setCurrency(currency);
        operation.setId(resultSet.getInt("currency_id"));
        operation.setOperationType(resultSet.getString("operation_type"));
        operation.setCommission(resultSet.getBigDecimal("commission"));
        transaction.setOperation(operation);
        return transaction;
    }

    @Override
    public void addAccountTransaction(Transaction transaction) throws SQLException {
        Connection connection = getConnection();
        PreparedStatement preparedStatement = null;
        String sql = "INSERT INTO transaction " +
                "(account_number_this,operation_commentary,date,sum,currency_id,operation_id) " +
                "VALUES (?,?,?,?,?,?)";
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setLong(1, transaction.getAccountNumberThis());
            preparedStatement.setString(2, transaction.getOperationCommentary());
            preparedStatement.setTimestamp(3, transaction.getDate());
            preparedStatement.setBigDecimal(4, transaction.getSum());
            preparedStatement.setInt(5, transaction.getCurrency().getId());
            preparedStatement.setInt(6, transaction.getOperation().getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) preparedStatement.close();
            if (connection != null) connection.close();
        }
    }
}
