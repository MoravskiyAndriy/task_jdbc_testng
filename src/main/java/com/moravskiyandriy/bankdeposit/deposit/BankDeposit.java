package com.moravskiyandriy.bankdeposit.deposit;

import com.moravskiyandriy.entities.Currency;

import java.math.BigDecimal;
import java.sql.Date;

public interface BankDeposit {
    int getUserId();

    BigDecimal getSum();

    Currency getCurrency();

    BigDecimal getRate();

    Date getOpeningDate();

    Date getClosingDate();
}
