package com.moravskiyandriy.bankdeposit.deposit;

import java.math.BigDecimal;

public class DepositConstants {
    private DepositConstants() {
    }

    public static final int SHORT_COMMON_DEPOSIT_TERM = 90;
    public static final int LONG_COMMON_DEPOSIT_TERM = 180;
    public static final int SHORT_BONUS_DEPOSIT_TERM = 90;
    public static final int LONG_BONUS_DEPOSIT_TERM = 180;
    public static final BigDecimal SHORT_BONUS = new BigDecimal(2);
    public static final BigDecimal LONG_BONUS = new BigDecimal(4);
    public static final BigDecimal LONG_RATE = new BigDecimal(15);
    public static final BigDecimal SHORT_RATE = new BigDecimal(10);
}
