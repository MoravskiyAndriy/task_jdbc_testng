package com.moravskiyandriy.bankdeposit.deposit.implementations;

import com.moravskiyandriy.bankdeposit.deposit.BankDeposit;
import com.moravskiyandriy.entities.Currency;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;

public class GeneralBankDeposit implements BankDeposit {
    private static final Logger logger = LogManager.getLogger(GeneralBankDeposit.class);
    protected int id;
    private int userId;
    protected BigDecimal sum;
    protected Currency currency;
    private BigDecimal rate;
    private int timeInDays;
    private Date openingDate;

    GeneralBankDeposit() {
    }

    GeneralBankDeposit(int userId, BigDecimal sum, Currency currency, Date openingDate) {
        this.userId = userId;
        this.sum = sum;
        this.currency = currency;
        this.openingDate = openingDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public BigDecimal getSum() {
        return sum;
    }

    public void setSum(BigDecimal sum) {
        this.sum = sum;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Date getOpeningDate() {
        return openingDate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setOpeningDate(Date openingDate) {
        this.openingDate = openingDate;
    }

    public int getTimeInDays() {
        return timeInDays;
    }

    public void setTimeInDays(int timeInDays) {
        this.timeInDays = timeInDays;
    }

    public Date getClosingDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        c.setTime(getOpeningDate());
        c.add(Calendar.DATE, timeInDays);
        return Date.valueOf(sdf.format(c.getTime()));
    }

    protected static Properties getProperties() {
        Properties prop = new Properties();
        try (InputStream input = BonusDepositLong.class
                .getClassLoader().getResourceAsStream("config")) {
            if (input != null) {
                prop.load(input);
            }
        } catch (NumberFormatException ex) {
            logger.warn("NumberFormatException found.");
        } catch (IOException ex) {
            logger.warn("IOException found.");
        }
        return prop;
    }
}
