package com.moravskiyandriy.controller.implementation;

import com.moravskiyandriy.businessLogic.Constants;
import com.moravskiyandriy.controller.BankController;
import com.moravskiyandriy.entities.*;
import com.moravskiyandriy.extraservice.Ticket;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Date;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.IntStream;
import static org.testng.Assert.*;

public class BankControllerTest {
    private static final Logger logger = LogManager.getLogger(BankControllerTest.class);
    private static final int TESTS_NUMBER = 1;
    private static User testUser;
    private static BankController bankController;
    private static List<Transaction> testTransactions;

    @BeforeClass
    private static void initialize() throws ParseException {
        Reporter.log("Initialization began...");
        testTransactions = new LinkedList<>();
        bankController = new BankControllerImpl();
        testUser = new User("Andriy", "Moravskiy", "Andriyovych",
                "myemail@gmail.com", "0631111111", "Zhowkva",
                "MyStreet", 5, 6, new ArrayList<>(), new ArrayList<>(), new ArrayList<>());
        testUser.setId(1);
        List<Account> testAccounts = new LinkedList<>();
        testAccounts.add(new Account(1, 1000000000000001L, new BigDecimal(1000).setScale(2, RoundingMode.HALF_UP)
                , new Currency(1, "UAH", new BigDecimal(1))));
        testAccounts.add(new Account(1, 1000000000000002L, new BigDecimal(5000).setScale(2, RoundingMode.HALF_UP)
                , new Currency(1, "UAH", new BigDecimal(1))));
        testUser.setAccounts(testAccounts);
        List<Credit> testCredits = new LinkedList<>();
        testCredits.add(new Credit(1, new BigDecimal(100).setScale(2, RoundingMode.HALF_UP),
                new Currency(1, "UAH", new BigDecimal(1)),
                new BigDecimal(20.00).setScale(2, RoundingMode.HALF_UP),
                Date.valueOf("2019-10-27")
                , Date.valueOf("2019-11-27")));
        testCredits.add(new Credit(1, new BigDecimal(20).setScale(2, RoundingMode.HALF_UP),
                new Currency(1, "UAH", new BigDecimal(1)),
                new BigDecimal(20.00).setScale(2, RoundingMode.HALF_UP), Date.valueOf("2019-10-27")
                , Date.valueOf("2019-12-27")));
        testUser.setCredits(testCredits);
        bankController.setUser(new AuthenticationControllerImpl().loginUser(testUser.getPhoneNumber(), "admin".toCharArray()));
        java.sql.Timestamp timestamp = java.sql.Timestamp.valueOf("2019-10-27 12:10:00");
        testTransactions.add(new Transaction(1, 1000000000000001L,
                "0631111111", timestamp, new BigDecimal(20).setScale(2, RoundingMode.HALF_UP),
                new Currency(1, "UAH", new BigDecimal(1).setScale(2, RoundingMode.HALF_UP)),
                new Operation(3, "replenishment of mobile account", new BigDecimal(1).setScale(2, RoundingMode.HALF_UP))));
        Reporter.log("Initialization ended...");
    }

    @Test
    void getAccountsTest() {
        Reporter.log("Trying to get accounts ");
        List<Account> accounts = bankController.getAccounts();
        assertEquals(accounts.toArray().length, testUser.getAccounts().toArray().length);
        IntStream.range(0, accounts.toArray().length).
                forEach(i -> assertEquals(accounts.get(i).getNumber(), testUser.getAccounts().get(i).getNumber()));
    }

    @Test
    void getDepositsTest() {
        Reporter.log("Trying to get deposits ");
        List<Deposit> deposits = bankController.getDeposits();
        assertEquals(deposits.toArray().length, testUser.getDeposits().toArray().length);
    }

    @Test
    void getCreditsTest() {
        Reporter.log("Trying to get credits ");
        List<Credit> credits = bankController.getCredits();
        assertEquals(credits.toArray().length, testUser.getCredits().toArray().length);
        IntStream.range(0, credits.toArray().length).
                forEach(i -> assertEquals(credits.toArray()[i].toString(), testUser.getCredits().toArray()[i].toString()));
    }

    @Test
    void replenishAccountBalanceTest() {
        Reporter.log("Trying to replenish Account Balance ");
        BigDecimal balance = bankController.getAccounts().get(0).getBalance();
        bankController.setCurrentAccount(bankController.getAccounts().get(0));
        bankController.replenishAccountBalance(new BigDecimal(100), Constants.UAH_Currency, "test replenishing.");
        assertEquals(0, balance.add(new BigDecimal(100)).setScale(2, RoundingMode.HALF_UP).
                compareTo(bankController.getAccounts().get(0).getBalance()));
        testUser.getAccounts().get(0).setBalance(testUser.getAccounts().get(0).
                getBalance().add(new BigDecimal(100).setScale(2, RoundingMode.HALF_UP)));
    }

    @Test
    void exhaustAccountBalanceTest() {
        Reporter.log("Trying to exhaust Account Balance ");
        BigDecimal balance = bankController.getAccounts().get(0).getBalance();
        bankController.setCurrentAccount(bankController.getAccounts().get(0));
        bankController.replenishPhoneBalance(new BigDecimal(10).setScale(2, RoundingMode.HALF_UP), "test exhausting.");
        assertEquals(0, balance.subtract(new BigDecimal(11)).setScale(2, RoundingMode.HALF_UP).
                compareTo(bankController.getAccounts().get(0).getBalance()));
    }

    @Test
    void getCurrencyByAbbreviationTest() {
        Reporter.log("Trying to get Currency By Abbreviation ");
        Currency UAHcurrency = Constants.UAH_Currency;
        UAHcurrency.setToUAHModifier(UAHcurrency.getToUAHModifier().setScale(5, RoundingMode.HALF_UP));
        Currency testCurrency = bankController.getCurrencyByAbbreviation("UAH");
        Assert.assertEquals(UAHcurrency.toString(), testCurrency.toString());
    }

    @Test
    void addAndDeleteAccountTest() {
        Reporter.log("Trying to add And Delete Account ");
        int number = bankController.getAccounts().size();
        bankController.addAccount(Constants.UAH_Currency);
        int testNumber = bankController.getAccounts().size();
        assertEquals(number + 1, testNumber);
        int number2 = bankController.getAccounts().size();
        bankController.setCurrentAccount(bankController.getAccounts().get(2));
        bankController.deleteAccount();
        int testNumber2 = bankController.getAccounts().size();
        assertEquals(number2 - 1, testNumber2);
    }

    @Test
    void EmptyAccountTest() {
        Reporter.log("Checking if account can be empty ");
        BigDecimal balance = testUser.getAccounts().get(0).getBalance().setScale(2, RoundingMode.HALF_UP);
        testUser.getAccounts().get(0).setBalance(new BigDecimal(0).setScale(2, RoundingMode.HALF_UP));
        bankController.setCurrentAccount(testUser.getAccounts().get(0));
        assertTrue(bankController.accountIsEmpty());
        testUser.getAccounts().get(0).setBalance(balance);
        bankController.setCurrentAccount(testUser.getAccounts().get(0));
        assertFalse(bankController.accountIsEmpty());
    }

    @Test
    void noUserTest() {
        Reporter.log("Checking identification of no user ");
        assertFalse(bankController.checkNoUser());
    }

    @Test
    void changeAccountCurrencyTest() {
        Reporter.log("Trying to change Account Currency ");
        bankController.setCurrentAccount(bankController.getAccounts().get(0));
        bankController.changeAccountCurrency(new Currency(2, "USD",
                new BigDecimal(0.04).setScale(5, RoundingMode.HALF_UP)));
        Currency testCurrency = bankController.getAccounts().get(0).getCurrency();
        assertEquals(testCurrency.toString(), new Currency(2, "USD",
                new BigDecimal(0.04).setScale(5, RoundingMode.HALF_UP)).toString());
        bankController.changeAccountCurrency(Constants.UAH_Currency);
        Currency testCurrency2 = bankController.getAccounts().get(0).getCurrency();
        assertEquals(testCurrency2.toString(), new Currency(1, "UAH", new BigDecimal(1).
                setScale(5, RoundingMode.HALF_UP)).toString());
    }

    @Test
    void buyTicketTest() {
        Reporter.log("Trying to buy Ticket ");
        bankController.setCurrentAccount(bankController.getAccounts().get(0));
        BigDecimal balance = bankController.getAccounts().get(0).getBalance().setScale(2, RoundingMode.HALF_UP);
        bankController.buyTicket(Ticket.TO_UKRAINE);
        BigDecimal testBalance = bankController.getAccounts().get(0).getBalance().setScale(2, RoundingMode.HALF_UP);
        assertEquals(0, balance.subtract(Ticket.TO_UKRAINE.getPrice().
                add(new BigDecimal(10).multiply(bankController.getAccounts().get(0).getCurrency().getToUAHModifier()).setScale(2, RoundingMode.HALF_UP))).compareTo(testBalance));
    }
}
