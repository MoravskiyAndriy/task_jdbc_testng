package com.moravskiyandriy.controller.implementation;

import com.moravskiyandriy.entities.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Reporter;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;

import static org.testng.Assert.*;

public class AuthenticationControllerTest {
    private static final Logger logger = LogManager.getLogger(AuthenticationControllerTest.class);
    private static User testUser;

    @BeforeClass
    private static void initialize() {
        Reporter.log("User created: ");
        //logger.info("User created: ");
        testUser = new User("Andriy", "Moravskiy", "Andriyovych",
                "myemail@gmail.com", "0631111111", "Zhowkva",
                "MyStreet", 5, 6, new ArrayList<>(), new ArrayList<>(), new ArrayList<>());
        testUser.setId(1);
        Reporter.log(testUser.toString());
        //logger.info(testUser.toString());
    }

    @Test
    void loginTest() {
        Reporter.log("Logging user...");
        //logger.info("Logging user...");
        User user = new AuthenticationControllerImpl().loginUser("0631111111", "admin".toCharArray());
        assertEquals(user.getId(), testUser.getId());
    }

    @Test(dataProvider = "phoneNumbersProvider")
    void rightUserTest(String phoneNumber) {
        Reporter.log("Phone number validation...");
        //logger.info("Phone number validation...");
        assertTrue(new AuthenticationControllerImpl().phoneNumberAlreadyExists(phoneNumber));
        assertFalse(new AuthenticationControllerImpl().phoneNumberAlreadyExists("badNumber"));
    }

    @DataProvider
    public Object[][] phoneNumbersProvider(){
        return new Object[][]{{"0631111111"},{"0632221111"},{"0633331111"}};
    }

}
